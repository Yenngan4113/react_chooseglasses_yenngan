import React, { Component, Fragment } from "react";
import style from "./BT.module.css";
import data from "./dataGlasses.json";

export default class BT extends Component {
  state = {
    imgUrl: "",
    display: "none",
    name: "",
    price: "",
    des: "",
  };
  handleChangeGlasses = (url, index) => {
    console.log(index, data[1].name);
    let glassName = data[index].name;
    let glassPrice = data[index].price;
    let glassDes = data[index].desc;
    this.setState({
      imgUrl: url,
      display: "block",
      name: glassName,
      price: glassPrice,
      des: glassDes,
    });
  };

  render() {
    return (
      <div
        style={{
          //   backgroundImage: `url("./img/background.jpg")`,
          height: "100vh",
        }}
      >
        <div
          class="bg-dark text-center text-white py-5"
          style={{ fontSize: "36px" }}
        >
          TRY GLASSES APP ONLINE
        </div>
        <div className={`${style.img} ${style.div}`}>
          <img src="./img/model.jpg" style={{ height: "400px" }} />
          <img
            style={{
              height: "60px",
            }}
            className={`${style.overlay}`}
            src={this.state.imgUrl}
          ></img>
          <div
            className="description"
            style={{
              height: "120px",
              width: "328.2px",
              backgroundColor: "rgba(163,165,164,0.5)",
              position: "absolute",
              top: "300px",

              display: `${this.state.display}`,
            }}
          >
            <h5
              className="text-warning mb-0 pl-3 mt-3"
              style={{ fontSize: "18px" }}
            >
              {this.state.name}
            </h5>
            <p className="text-danger m-0 pl-3" style={{ fontSize: "16px" }}>
              Price: {this.state.price}$
            </p>
            <p className="text-success pl-3" style={{ fontSize: "14px" }}>
              {this.state.des}
            </p>
          </div>
        </div>

        <div style={{ width: "400px", margin: "auto" }} className="row ">
          {data.map((item, index) => {
            return (
              <div
                className="col-4 d-flex justify-content-center"
                onClick={() => {
                  this.handleChangeGlasses(`${item.url}`, index);
                }}
              >
                <img
                  src={item.url}
                  style={{
                    width: "100px",
                    marginBottom: "30px",
                  }}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
